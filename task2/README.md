## Создайте web-сервис хранения переменных
___


### Цель
> Ваша цель состоит в том, чтобы разработать web-сервис хранения переменных.  
> Для хранения данных использовать файлы.  
> Ниже описаны роуты для Вашего веб сервиса

<br>


### 1. Создание переменной для сервиса

**Структура запроса**

> POST: /api/v1/`<serviceName>`/

```json
{
  "variableName": "<variableName>",
  "variableValue": "<variableValue>"
}
```


**Структура ответа:**

```json
{
  "serviceName": "<createdServiceName>",
  "variableName": "<createdVariableName>",
  "variableValue": "<createdVariableValue>"
}
```
В случае успеха - StatusCode - 201  
Если такого `<serviceName>` еще нет - должен быть создан


**Пример запроса**  

> POST: /api/v1/amplitude-homebank/

```json
{
  "variableName": "start-date",
  "variableValue": "20221026T13"
}
```

**Пример ответа**  

```json
{
  "serviceName": "amplitude-homebank",
  "variableName": "start-date",
  "variableValue": "20221026T13"
}
```
StatusCode - 201

<br>


### 2. Получение значения переменной от сервиса

**Структура запроса**

> GET: /api/v1/`<serviceName>`/`<variableName>`/


**Структура ответа:**

```json
{
  "serviceName": "<serviceName>",
  "variableName": "<variableName>",
  "variableValue": "<variableValue>"
}
```
В случае успеха StatusCode - 200  
Если переменная не найдена, то StatusCode - 404


**Пример запроса**  

> GET: /api/v1/amplitude-homebank/start-date/


**Пример ответа**  
```json
{
    "serviceName": "amplitude-homebank",
    "variableName": "start-date",
    "variableValue": "20221026T12"
}
```
StatusCode - 200


<br>


### 3. Удаление переменной сервиса


**Структура запроса**

> DELETE: /api/v1/`<serviceName>`/`<variableName>`/


**Структура ответа:**

Достаточно вернуть только statuscode (тела - нет)
В случае успеха StatusCode - 204 
Если переменная у сервиса не найдена, то StatusCode - 404


**Пример запроса**  

> DELETE: /api/v1/amplitude-homebank/start-date/


**Пример ответа**  

StatusCode - 204  

<br>


### 4. Изменение значения переменной конкретного сервиса

**Структура запроса**

> PATCH: /api/v1/`<serviceName>`/`<variableName>`/

```json
{
  "variableValue": "<newVariableValue>"
}
```

**Структура ответа:**

```json
{
  "serviceName": "<serviceName>",
  "variableName": "<variableName>",
  "variableValue": "<newVariableValue>"
}
```
В случае успеха StatusCode - 200  
Если сервис с переменной не найден, то StatusCode - 404


**Пример запроса**  

> PATCH: /api/v1/amplitude-homebank/start-date/

```json
{
  "variableValue": "20221026T13"
}
```

**Пример ответа**  

```json
{
  "serviceName": "amplitude-homebank",
  "variableName": "start-date",
  "variableValue": "20221026T13"
}
```
StatusCode - 200
